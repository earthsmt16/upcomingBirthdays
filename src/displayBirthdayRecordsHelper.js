import {BirthdayRecords} from "./birthdayRecords.js";

export function updateViews() {
  var records = BirthdayRecords.getAllRecords()
  console.log(records)
  var parsedRecords = JSON.parse(records)
  console.log(parsedRecords)
  updateBirthdayListView(parsedRecords)
  updateTodayBirthdaysView(parsedRecords)
  updateUpcomingBirthdaysView(parsedRecords)
}

export function removeRecordAndUpdateViews() {
  var dataId = this.getAttribute('data-id')
  var dataParts = dataId.split("_");
  var month = dataParts[0]
  var uniqueIdentifierInMonth = dataParts[1]
  var records = BirthdayRecords.getAllRecords()
  var parsedRecords = JSON.parse(records)
  var parsedRecordsWithRemovedRecord = BirthdayRecords.removeRecord(
    parsedRecords,
    month,
    uniqueIdentifierInMonth
  )
  updateBirthdayListView(parsedRecordsWithRemovedRecord)
  updateTodayBirthdaysView(parsedRecordsWithRemovedRecord)
  updateUpcomingBirthdaysView(parsedRecordsWithRemovedRecord)
}

export function updateViewsFilteredByKeyword() {
  var records = BirthdayRecords.getAllRecords()
  if (records) {
    console.log(records)
    var parsedRecords = JSON.parse(records)
    console.log(parsedRecords)
    var keyword = document.getElementsByName('search')[0].value
    parsedRecords = filterRecordsByKeyword(parsedRecords, keyword)
    updateBirthdayListView(parsedRecords)
    updateTodayBirthdaysView(parsedRecords)
    updateUpcomingBirthdaysView(parsedRecords)
  }
}

export function updateBirthdayListView(parsedRecords) {
  var maxNumRecords = 9
  clearTables('BirthdayList_', maxNumRecords)
  var maxNumRecordsToView = maxNumRecords
  var currentMonth = getCurrentMonth()
  var currentDate = getCurrentDate()
  var monthWorkingOn = currentMonth
  var startIndex = 0
  do {
    var records = parsedRecords[monthWorkingOn - 1]
    if (monthWorkingOn == currentMonth) {
      records = filterRecordsByDates(currentDate, 31, records)
    }
    if (records) {
        var numResultsAddedToView = displayRecords('BirthdayList_', records, startIndex, maxNumRecordsToView)
    }
    startIndex += numResultsAddedToView
    maxNumRecordsToView -= numResultsAddedToView
    if (maxNumRecordsToView == 0) {
      break
    }
    monthWorkingOn = (monthWorkingOn == 13 ? 1 : monthWorkingOn + 1);
  } while (monthWorkingOn != currentMonth)

  var records = parsedRecords[currentMonth - 1]
  records = filterRecordsByDates(1, currentDate - 1, records)
  var numResultsAddedToView = displayRecords('BirthdayList_', records, startIndex, maxNumRecordsToView)
}

export function updateTodayBirthdaysView(parsedRecords) {
  var maxNumRecordsToView = 3
  clearTables('TodaysBirthdays_', maxNumRecordsToView)
  var records = parsedRecords[getCurrentMonth() - 1]
  if (records) {
      var currentDate = getCurrentDate()
      var records = filterRecordsByDates(currentDate, currentDate, records)
      displayRecords('TodaysBirthdays_', records, 0, maxNumRecordsToView)
  }
}

export function updateUpcomingBirthdaysView(parsedRecords) {
  var maxNumRecordsToView = 3
  clearTables('UpcomingBirthdays_', maxNumRecordsToView)
  var nextDay = getDatePartsForFutureDay(1)
  var fromMonth = nextDay['month']
  var records = parsedRecords[fromMonth - 1]
  if (records) {
      var fromDate = nextDay['date']
      var dateInfoTwoWksTime = getDatePartsForFutureDay(14)
      var dateTwoWksTime = dateInfoTwoWksTime['date']
      var monthTwoWksTime = dateInfoTwoWksTime['month']

      var checkNextMonth = false
      var dateTo = Math.min(fromDate + 13, 31)
      if (monthTwoWksTime != fromMonth) {
        checkNextMonth = true
      }
      var records = filterRecordsByDates(fromDate, dateTo, records)
      var numResultsAddedToView = displayRecords('UpcomingBirthdays_', records, 0, maxNumRecordsToView)

      if (checkNextMonth) {
        var records = parsedRecords[monthTwoWksTime - 1]
        records = filterRecordsByDates(1, dateTwoWksTime, records)
        maxNumRecordsToView -= numResultsAddedToView
        numResultsAddedToView = displayRecords('UpcomingBirthdays_', records, numResultsAddedToView, maxNumRecordsToView)
      }
  }
}

export function getCurrentMonth() {
  return new Date().getMonth() + 1;
}

export function getCurrentDate() {
  return new Date().getDate();
}

export function getDatePartsForFutureDay(day) {
  var milliseconds = new Date().getTime()
  var millsecondsFuture = milliseconds + (60 * 60 *24 * day * 1000)
  var dateObjFuture = new Date(millsecondsFuture)

  var dateParts =[]
  dateParts['date'] = dateObjFuture.getDate()
  dateParts['month'] = dateObjFuture.getMonth() + 1
  return dateParts
}

export function displayRecords(tableId, records, startIndex, maxNumRecordsToView) {
  var numRecords = Object.keys(records).length
  var numRecordsToAddToView = Math.min(numRecords, maxNumRecordsToView)
  for (let i = 1; i <= numRecordsToAddToView; i++) {
    var record = records[i -1];
    var tableIndex = startIndex + i
    document.getElementById(tableId.concat(tableIndex)).innerHTML =
      record['firstName'].concat(' ').
      concat(record['lastName']).concat(', ').
      concat(record['birthday_in_textual']).concat(', Age: ').
      concat(record['age']) . concat('&nbsp;'.repeat(10)) .
      concat('<button name="removeBirthday">Remove</button>')
    document.getElementById(tableId.concat(tableIndex)).setAttribute("data-id", record["id"])
    document.getElementById(tableId.concat(tableIndex)).addEventListener("click", removeRecordAndUpdateViews);
  }
  return numRecordsToAddToView
}

export function clearTables(tableId, numRecords) {
  for (let i = 1; i <= numRecords; i++) {
    document.getElementById(tableId.concat(i)).innerHTML = ''
  }
}

export function filterRecordsByDates(dateFrom, dateTo, records) {
  var filteredRecords = {}
  var filteredRecordIndex = 0
  var numRecords = Object.keys(records).length
  for (let i = 1; i <= numRecords; i++) {
    var record = records[i -1];
    var birthDate = new Date(record['birthday']).getDate();
    if (birthDate >= dateFrom && birthDate <= dateTo) {
        filteredRecords[filteredRecordIndex] = record
        filteredRecordIndex++
    }
  }
  return filteredRecords
}

export function filterRecordsByKeyword(records, keyword) {
  for(var i in records) {
    var recordsArr = []
    for(var j in records[i]) {
      if (
        records[i][j]["firstName"].toLowerCase().search(keyword.toLowerCase()) >= 0 ||
        records[i][j]["lastName"].toLowerCase().search(keyword.toLowerCase()) >= 0
      ) {
         recordsArr.push(records[i][j])
      }
    }
    records[i] = Object.fromEntries(Object.entries(recordsArr).map(([key, value]) => [key, value]))
  }
  return records
}
