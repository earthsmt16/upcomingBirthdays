
import {Person} from "./person.js";

export class BirthdayRecords {
  constructor() {
    if (!(BirthdayRecords._instance)) {
      BirthdayRecords.initializeRecords();
    }
    BirthdayRecords._instance = this;
  }

  static submitRecord() {
    var firstName = document.getElementsByName('fname')[0].value
    var lastName = document.getElementsByName('lname')[0].value
    var dob = document.getElementsByName('birthday')[0].value

    var person = new Person(firstName, lastName, dob)
    var month = person.getPersonDobMonth()
    var parsedRecordsMonthIndex = month - 1

    var records = BirthdayRecords.getAllRecords()
    var parsedRecords = records ? JSON.parse(records) : [];
    var recordsForMonth = parsedRecords[parsedRecordsMonthIndex]
    var numRecordsForMonth = BirthdayRecords.#numRecords(recordsForMonth)

    var attributes = person.getRequiredAttributes()

    parsedRecords[parsedRecordsMonthIndex][numRecordsForMonth] = {
      "id": parsedRecordsMonthIndex.toString().concat('_').concat(numRecordsForMonth),
      "firstName": attributes['firstName'],
      "lastName": attributes['lastName'],
      "birthday": attributes['dob'],
      "birthday_timestamp": attributes['dobTimestamp'],
      "birthday_in_textual": attributes['textual_dob'],
      "date" : new Date(attributes['dobTimestamp']* 1000).getDate(),
      "age" : attributes['age']
    };
    parsedRecords[parsedRecordsMonthIndex] = BirthdayRecords.#sortRecords(
      parsedRecords[parsedRecordsMonthIndex]
    );
    BirthdayRecords.#updateRecords(parsedRecords);
  }

  static initializeRecords() {
    var records = {};
    for (let i = 0; i < 12; i++) {
      records[i] = {}
    }
    BirthdayRecords.#updateRecords(records)
  }

  static #updateRecords(records) {
    document.getElementsByName('birthdayData')[0].innerHTML = JSON.stringify(records);
  }

  static #numRecords(records) {
    return Object.keys(records).length;
  }

  static #sortRecords(records) {
    var recordsArr = []
    for(var i in records) {
        recordsArr.push(records[i]);
    }
    recordsArr = recordsArr.sort(function(a, b) {
        return a['date'] - b['date']
    });
    return Object.fromEntries(Object.entries(recordsArr).map(([key, value]) => [key, value]))
  }

  static removeRecord(records, month, uniqueRecordId) {
    var recordsArr = []
    for(var i in records[month]) {
        recordsArr.push(records[month][i]);
    }
    recordsArr.splice(uniqueRecordId, 1)
    for (let i = 0; i < recordsArr.length; i++) {
      recordsArr[i]['id'] = month.toString().concat('_').concat(i)
    }
    records[month] = Object.fromEntries(Object.entries(recordsArr).map(([key, value]) => [key, value]))
    records[month] = BirthdayRecords.#sortRecords(records[month]);
    BirthdayRecords.#updateRecords(records)
    return records
  }

  static getAllRecords() {
    return document.getElementsByName('birthdayData')[0].innerHTML
  }
}
