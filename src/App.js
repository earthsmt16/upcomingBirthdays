import logo from './logo.svg';
import './App.css';
import React, {useState} from 'react';
import Modal from "./modal";
import {updateViews, updateViewsFilteredByKeyword} from "./displayBirthdayRecordsHelper.js";

function App() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div className="App">
      <button type="button" onClick={() => setIsOpen(true)} name='addBirthdayButton'>Add</button>
      <input type="text" id="search" name="search" placeholder="keyword" />
      <button type="button" onClick={updateViewsFilteredByKeyword} name='submitSearch'>Filter</button>
      <button type="button" onClick={updateViews} name='showAll'>Show All</button>
      <BirthdayTable header="Birthday List" numRows='9'/>
      <BirthdayTable header="Today's Birthdays" numRows='3'/>
      <BirthdayTable header="Upcoming Birthdays" numRows='3'/>
      {isOpen && <Modal setIsOpen={setIsOpen} />}
      <script name="birthdayData" type="application/json"></script>
    </div>
  );
}

function BirthdayTable(birthdayTable) {
  var tableHeader = birthdayTable.header
  var tableName = birthdayTable.header.replace(/\s/g, '').replace("'",'')
  return <table id={tableName}>
    <tr>
      <th>{tableHeader}</th>
    </tr>
    {(() => {
        let elements = []
        for (let i = 1; i <= birthdayTable.numRows; i++) {
          elements.push(
            <tr>
              <td id={tableName.concat('_').concat(i)}></td>
            </tr>
          )
        }
        return elements;
    })()}
  </table>;
}

export default App;
