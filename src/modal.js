import React from "react";
import styles from "./modal.css";
import { RiCloseLine } from "react-icons/ri";
import {BirthdayRecords} from "./birthdayRecords.js";
import {updateViews} from "./displayBirthdayRecordsHelper.js";

const Modal = ({ setIsOpen }) => {
  function submitRecord() {
      new BirthdayRecords()
      BirthdayRecords.submitRecord()
      updateViews()
      setIsOpen(false)
  }
  return (
    <div class="popup-box">
      <div class="box">
        <label for="name">First Name:</label>
        <input type="text" id="fname" name="fname" /><br /><br />
        <label for="name">Last Name:</label>
        <input type="text" id="lname" name="lname" /><br /><br />
        <label for="Birthday">Birthday:</label>
        <input type="date" name="birthday" /><br /><br />
        <button type="button" onClick={() => setIsOpen(false)} name='cancel'>Cancel</button>
        <button type="button" onClick={submitRecord} name='test'>Submit</button>
      </div>
    </div>
  )
};

export default Modal;
