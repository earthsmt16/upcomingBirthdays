
export class Person {
  constructor(firstName, lastName, dob) {
    this.firstName = firstName
    this.lastName = lastName
    this.dob = dob
    this.#generateOtherAttributes()
  }

  #generateOtherAttributes() {
    this.month = this.#getMonthFromDate(this.dob)
    this.dobTimestamp = this.#getTimeStampFromDate(this.dob)
    this.textualVersionOfDob = this.#getTextualVersionOfDate(this.dob)
    this.age = this.#calculateAge(this.dob)
  }

  #getMonthFromDate(date) {
    const dateObj = new Date(date)
    return dateObj.getMonth() + 1;
  }

  #getTextualVersionOfDate(date) {
    const dateObj = new Date(date)
    return dateObj.toString().split(' ').slice(0, 4).join(' ')
  }

  #getTimeStampFromDate(date) {
    const dateObj = new Date(date)
    return dateObj.getTime()/1000
  }

  #calculateAge(date) {
    var currentDate = new Date()
    var customDate = new Date(date)
    var currentDateYear = currentDate.getYear()
    var customDateYear = customDate.getYear()
    var currentDateMonth = currentDate.getMonth()
    var customDateMonth = customDate.getMonth()
    var currentDateDate = currentDate.getDate()
    var customDateDate = customDate.getDate()

    var calculatedAge = currentDateYear - customDateYear
    if (
      (customDateMonth > currentDateMonth) ||
      (
        (customDateMonth == currentDateMonth) &&
        (customDateDate > currentDateDate)
      )
    ) {
      calculatedAge--
    }
    return calculatedAge
  }

  getPersonDobMonth() {
    return this.month
  }

  getRequiredAttributes() {
    var attributes = [];
    attributes['firstName'] = this.firstName
    attributes['lastName'] = this.lastName
    attributes['dob'] = this.dob
    attributes['dobTimestamp'] = this.dobTimestamp
    attributes['textual_dob'] = this.textualVersionOfDob
    attributes['age'] = this.age
    return attributes;
  }
}
