UI to show list of birthdays - including those today's and upcoming (within the next 2 weeks).

Please download the contents of this repo. Once downloaded, within the base folder, type: `npm start` - this will
start the birthday UI locally (http://localhost:3000).

Upon success, you will see the following:

![Alt text](https://gitlab.com/earthsmt16/upcomingBirthdays/uploads/83d382aa6dea25834e4efe6bd225006a/Screenshot_2022-05-03_at_07.40.08.png)
